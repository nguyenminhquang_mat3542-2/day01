CREATE DATABASE QLSV;

USE QLSV;

CREATE TABLE DMKHOA (
    MaKH VARCHAR(6) PRIMARY KEY,
    TenKhoa VARCHAR(30)
);

CREATE TABLE SINHVIEN (
    MaSV VARCHAR(6) PRIMARY KEY,
    HoSV VARCHAR(30),
    TenSV VARCHAR(15),
    Gioitinh CHAR(1),
    Ngaysinh DATETIME,
    Noisinh VARCHAR(50),
    Diachi VARCHAR(50),
    MaKH VARCHAR(6),
    Hocbong INT,
    FOREIGN KEY (MaKH) REFERENCES DMKHOA(MaKH)
);

INSERT INTO DMKHOA (MaKH, TenKhoa)
VALUES
    ('K001', 'Toan'),
    ('K002', 'Cong nghe thong tin'),
    ('K003', 'Ngon ngu anh');

INSERT INTO SINHVIEN (MaSV, HoSV, TenSV, Gioitinh, Ngaysinh, Noisinh, Diachi, MaKH, Hocbong)
VALUES
    ('SV001', 'Nguyen', 'Minh Quang', 'M', '2000-01-15', 'Ha Noi', 'Minh Khai', 'K001', 5000000),
    ('SV002', 'Hoang', 'Chi Quang', 'M', '1999-04-20', 'Ho Chi Minh', 'Hoai Duc', 'K002', 6000000),
    ('SV003', 'Nguyen', 'Dac Quang', 'M', '2001-03-10', 'Da Nang', 'Hoai Duc', 'K001', 5500000);
