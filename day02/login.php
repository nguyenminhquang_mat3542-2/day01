<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');

$daysOfWeek = array(
    'Chủ Nhật',
    'Thứ Hai',
    'Thứ Ba',
    'Thứ Tư',
    'Thứ Năm',
    'Thứ Sáu',
    'Thứ Bảy'
);
$dayOfWeek = $daysOfWeek[date('w')];
$current_time = date('H:i:s');
$current_day = date('d-m-y');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Đăng nhập</h2>
    
    <p>Bây giờ là: <?php echo $current_time; ?> <?php echo $dayOfWeek; ?> ngày: <?php echo $current_day; ?></p>
    
    <form method="post" action="process_login.php">
        <label for="username">Tên đăng nhập:</label>
        <input type="text" name="username" id="username" required><br>
        
        <label for="password">Mật khẩu:</label>
        <input type="password" name="password" id="password" required><br>
        
        <input type="submit" value="Đăng nhập">
    </form>
</body>
</html>